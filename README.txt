CS 418 MP3 README

by:
Michael Fong
fong11

To open the project you'll need Microsoft Visual Studio 2013. 
Open the mp3.sln file within the Source folder to run the project. 

Controls:
s : switch between environment and texture mapping (default is environment)
a : rotate around 1,1,1
r : reset to original position
ESC : quit

<- : rotate to the left
-> : rotate to the right
up arrow : pitch teapot up
down arrow: pitch teapot down

Youtube:
http://youtu.be/Jr9byVvoIK0