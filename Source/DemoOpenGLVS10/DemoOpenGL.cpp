#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <GL/glew.h>
#include <GL/glut.h>

#include "gfx/vec3.h"
#include "gfx/mat3.h"

#define PI 3.14159265

using namespace gfx; // namespace for support vector/matrix functions
using namespace std; // namespace for C++ standard template library

//fps set to 30
int nFPS = 30;
//rotation angles
float xRot = 0, yRot = 0, dRot = 0;

struct MeshData
{
	//holds the vertices, faces, and their normals
	vector<GLfloat> vert;
	vector<GLfloat> face;
	
	vector<GLshort> faceVert;
	vector<GLfloat> faceNormal;

	vector<GLfloat> vertNormal;
	vector<GLfloat> vertTextureCoord;
	
	//total number of faces and vertices
	int numVert;
	int numFace;

	//normalize one vector
	void normalize(float* x){
		float mag = 0;
		int i;
		//calculate magnitude
		for(i = 0; i < 3; i++)
			mag += pow(x[i],2);
		mag = sqrt(mag);

		if(mag == 0)
			return;

		//normalize
		for(i = 0; i < 3; i++)
			x[i] /= mag;
	}
	//calculate cross product returned in xproduct
	void crossProduct(float x[], float y[], float* xproduct){
		xproduct[0] = x[1]*y[2] - y[1]*x[2];
		xproduct[1] = y[0]*x[2] - x[0]*y[2];
		xproduct[2] = x[0]*y[1] - y[0]*x[1];
	}
	/*
	puts the face values into the face array
	*/
	void insertFace(string line){
		int end = 2;
		int begin, i;
		string temp;
		for(i = 0; i < 3; i++){
			begin = end+1;
			if(i < 2)
				end = line.find(" ",begin);
			else
				end = line.size();
		
			temp = line.substr(begin, end - begin);
			face.push_back((float) (atoi(temp.c_str()) - 1));
		}
		//initializes the face normal array
		for(i = 0; i < 3; i++)
			faceNormal.push_back(0.0);
	}
	/*
	puts the vector values into the vector array
	*/
	void insertVert(string line){
		int end = 1;
		int begin,i;
		string temp;
		for(i = 0; i < 3; i++){
			begin = end+1;
			if(i < 2)
				end = line.find(" ",begin);
			else
				end = line.size();
			temp = line.substr(begin, end - begin);;
			//convert to float
			vert.push_back((float) atof(temp.c_str()));
		}
		//initializes the normals and then the texturecoords
		for(i = 0; i < 3; i++)
			vertNormal.push_back(0.0);
		for(i = 0; i < 2; i++)
			vertTextureCoord.push_back(0.0);
	}
	//calculate face and vertex normals
	void calculateNormals(){
		float x[3],y[3];
		int temp[3];
		int i, j, faceNum, vertNum;

		//each face
		for(faceNum = 0; faceNum < numFace; faceNum++){
			//STEP 1: calculate face normal
			//find each vertex associated with each face, store the vertex number in vert array
			for(vertNum = 0; vertNum < 3; vertNum++)
				temp[vertNum] = (int) face[3 * faceNum + vertNum];
			//calculate edge vectors
			for(i = 0; i < 3; i++){
				y[i] = vert[3*temp[1] + i]-vert[3*temp[0] + i];
				x[i] = vert[3*temp[2] + i]-vert[3*temp[0] + i];
			}
			//take cross product and store in appropriate slot
			crossProduct(x, y, &faceNormal[3 * faceNum]);
			normalize(&faceNormal[3 * faceNum]);
			//Now we have the face normal
			//STEP 2: add face normal to each vertex in that face
			for(i = 0; i < 3; i++){
				for(j=0; j < 3; j++){
					vertNum = face[3 * faceNum + i];
					vertNormal[3*vertNum + j] += faceNormal[3 * faceNum + j];
				}
			}
		}
		//now normalize the vertex normals
		for(vertNum = 0; vertNum < numVert; vertNum++)
			normalize(&vertNormal[3*vertNum]);
	}
	//map vertex coord to texture coord with cylindrical coordinates
	void calculateTextureCoord(){
		float maxY = -100, minY = 100, currY, yRange;
		unsigned int i, counter;
		counter = 0;
		for(i = 1; i < vert.size(); i += 3){
			currY = vert[i];
			if(currY > maxY)
				maxY = currY;
			if(currY < minY)
				minY = currY;
		}
		yRange = maxY - minY;
		float x,y,z,theta,s,t;
		for(i = 0; i < vert.size(); i += 3){
		//convert (x,y,z) to (r*cos(theta), y, r*sin(theta))
			x = vert[i];
			y = vert[i+1];
			z = vert[i+2];

			theta = atan2(z,x);
		
			s = (theta+PI)/(2*PI);
			t = y / yRange;
		//insert result back into array

			vertTextureCoord[counter] = s;
			vertTextureCoord[counter+1] = t;
			counter += 2;
		}
		return;
	}
	//load vertices and faces from a file and insert into corresponding vectors
	void loadObj(char* filename)
	{
		
		numVert = 0;
		numFace = 0;
		string line;

		ifstream teapot(filename);
		if (!teapot.is_open())
		{
			cout << "can't open object file"; 
			return;
		}
		//get the file line by line and process it
		while (teapot.good())
		{
			getline (teapot,line);
			if(line.size() < 1)
				continue;
			else if(line[0] == 'v')
				insertVert(line);
			else if(line[0] == 'f')
				insertFace(line);
		}
		numVert = vert.size() / 3;
		numFace = face.size() / 3;

		teapot.close();

		calculateNormals();
		calculateTextureCoord();

		//need to convert to short for glDrawElements
		for(int i = 0; i < face.size(); i++)
		{
			faceVert.push_back( (GLshort) face[i]);
		}
	}
	void draw() 
	{ 
			
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		//set vertex, normal, and texture pointers
		glVertexPointer(3, GL_FLOAT, 0, &(vert[0]));
		glNormalPointer(GL_FLOAT,0, &(vertNormal[0]));
		glTexCoordPointer(2, GL_FLOAT, 0, &(vertTextureCoord[0]));

		glDrawElements(GL_TRIANGLES, faceVert.size(), GL_UNSIGNED_SHORT, &(faceVert[0]));

		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	}
};





/*    
      readppm.c
      Nate Robins, 1997, 2000
      nate@pobox.com, http://www.pobox.com/~nate
 
      PPM File reader.  
*/
unsigned char* readPPM(char* filename, int* width, int* height)
{
    FILE* fp;
    int i, w, h, d;
    unsigned char* image;
    char head[70];          /* max line <= 70 in PPM (per spec). */
    
    fp = fopen(filename, "rb");
    if (!fp) {
        perror(filename);
        return NULL;
    }
    
    /* grab first two chars of the file and make sure that it has the
       correct magic cookie for a raw PPM file. */
    fgets(head, 70, fp);
    if (strncmp(head, "P6", 2)) {
        fprintf(stderr, "%s: Not a raw PPM file\n", filename);
        return NULL;
    }
    
    /* grab the three elements in the header (width, height, maxval). */
    i = 0;
    while(i < 3) {
        fgets(head, 70, fp);
        if (head[0] == '#')     /* skip comments. */
            continue;
        if (i == 0)
            i += sscanf(head, "%d %d %d", &w, &h, &d);
        else if (i == 1)
            i += sscanf(head, "%d %d", &h, &d);
        else if (i == 2)
            i += sscanf(head, "%d", &d);
    }
    
    /* grab all the image data in one fell swoop. */
    image = (unsigned char*)malloc(sizeof(unsigned char)*w*h*3);
    fread(image, sizeof(unsigned char), w*h*3, fp);
    fclose(fp);
    
    *width = w;
    *height = h;
    return image;
}

MeshData g_Mesh;

void setUpTexture(char * textureFile){
	
	GLuint textureNum;
	glEnable( GL_TEXTURE_2D );
	// allocate a texture name
	glGenTextures(1, &textureNum);
	// select our current texture
	glBindTexture(GL_TEXTURE_2D, textureNum);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);

	int width, height;
	unsigned char* texel = readPPM(textureFile, &width, &height);
	if(texel == NULL)
		exit(1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texel);
	
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	free(texel);
}
void init(void)
{
	// init your data, setup OpenGL environment here
	glClearColor(0.5,0.5,0.5,1.0); // gray
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);

	GLfloat lpos[] = {1.0,-10.0,-6.0,0};
	GLfloat La[] = {0.0,0.0,0.0,1.0};
	GLfloat Lid[] = {1.0,1.0,1.0,1.0};
	GLfloat Lis[] = {1.0,1.0,1.0,1.0};

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT0, GL_POSITION, lpos);
	glLightfv(GL_LIGHT0, GL_AMBIENT, La);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, Lid);
	glLightfv(GL_LIGHT0, GL_SPECULAR, Lis);

	GLfloat ka[] = {0.0,0.0,0.0,1.0};
	GLfloat kd[] = {1.0,1.0,1.0,1.0};
	GLfloat ks[] = {1.0,1.0,1.0,1.0};

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ka);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, kd);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, ks);
	glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,50.0);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	setUpTexture("sky.ppm");
	g_Mesh.loadObj("teapot_0.obj");
}

void display(void)
{

	// put your OpenGL display commands here
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// reset OpenGL transformation matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(0.f,1.5,7.f,0.f,1.5,0.f,0.f,1.f,0.f);
	glRotatef(yRot, 1,0,0);
	glRotatef(xRot, 0,1,0);
	glRotatef(dRot, 1,1,1);

	g_Mesh.draw();
	
	glutSwapBuffers();
}

void reshape (int w, int h)
{
	// reset viewport ( drawing screen ) size
	glViewport(0, 0, w, h);
	float fAspect = ((float)w)/h;
	// reset OpenGL projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70.f,fAspect,0.001f,30.f);
}

void keyboard(unsigned char key, int x, int y)
{
	//switches between environment and texture mapping
	static bool mappingSwitch = true;

	switch(key){
	case 27: 
		// ESC hit, so quit
		printf("Quit\n");
		exit(0);
		break;
	//environment / texture mapping
	case 's':
		if(mappingSwitch){
			glDisable(GL_TEXTURE_GEN_S);
			glDisable(GL_TEXTURE_GEN_T);
		}
		else {
			glEnable(GL_TEXTURE_GEN_S);
			glEnable(GL_TEXTURE_GEN_T);
		}
		mappingSwitch = !mappingSwitch;
		break;
	//a and s control the teapots rotation about (1,1,1)
	case 'a':
		dRot += 3;
		break;
	//reset the teapot to its original position
	case 'r':
		dRot = xRot = yRot = 0;
	}

}

void special(int key, int x, int y)
{	
	//rotate the teapot
	switch(key){
	case GLUT_KEY_LEFT:
		xRot -= 5;
		break;
	case GLUT_KEY_RIGHT:
		xRot += 5;
		break;
	case GLUT_KEY_DOWN:
		yRot += 5;
		break;
	case GLUT_KEY_UP:
		yRot -= 5;
		break;
	}
}


void timer(int v)
{
	glutPostRedisplay(); // trigger display function by sending redraw into message queue
	glutTimerFunc(1000/nFPS,timer,v); // restart timer again
}

int main(int argc, char* argv[])
{
	glutInit(&argc, (char**)argv);
	// set up for double-buffering & RBF color buffer & depth test
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (500, 500); 
	glutInitWindowPosition (100, 100);
	glutCreateWindow ((const char*)argv[0]);

	// init glew
	glewInit();
	if (glewIsSupported("GL_VERSION_2_0"))
		printf("Ready for OpenGL 2.0\n");
	else {
		printf("OpenGL 2.0 not supported\n");
		exit(1);
	}
	init(); // setting up user data & OpenGL environment
	
	glutDisplayFunc(display); 
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);	
	glutTimerFunc(100,timer,nFPS); // a periodic timer. Usually used for updating animation
	glutSpecialFunc(special);
	glutMainLoop();

	return 0;
}

